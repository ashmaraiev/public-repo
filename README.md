========= Test Devfile ============

Issue: https://issues.redhat.com/browse/CRW-7264

PR: https://github.com/eclipse-che/che-server/pull/737
